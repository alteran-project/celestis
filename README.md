# Celestis

An ActivityPub server that supports Server to Server and Client to Server APIs; for a single-user activity agnostic home in the Fediverse.

## Goals

1. Private activities will be stored in a Git repository
   1. This will be flat file YAML based, to allow:
      1. Content to be consumed by static site generators
      2. Data is free to move from system to system
2. Public activities ...
3. Objects for private activities will be stored in multiple configurable storage backends
   1. IPFS
   2. S3/GCS/ACS
