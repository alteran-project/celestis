use actix_rt;
use actix_web::web;
use actix_web::{App, HttpRequest, HttpServer, Responder};

mod actor;
mod well_known;

async fn index(_req: HttpRequest) -> impl Responder {
    format!("Hello, I am Celestis!")
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route(
                "/.well-known/webfinger",
                web::get().to(well_known::webfinger::get),
            )
            .route("/", web::get().to(index))
    })
    .bind("0.0.0.0:10000")?
    .run()
    .await
}
