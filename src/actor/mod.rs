pub struct Owner {
    pub name: String,
    pub email: String,
}

pub fn get_from_environment() -> Owner {
    Owner {
        name: std::env::var("OWNER_NAME").unwrap(),
        email: std::env::var("OWNER_EMAIL").unwrap(),
    }
}
