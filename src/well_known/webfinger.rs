use actix_web::http::StatusCode;
use actix_web::web;
use actix_web::{HttpResponse, Result};
use serde::{Deserialize, Serialize};

use crate::actor;

#[derive(Clone, Debug, Deserialize, Serialize)]
struct WebFinger {
	pub aliases: Vec<String>,
	pub links: Vec<String>,
	pub subject: String,
}

#[derive(Deserialize)]
pub struct WebFingerRequest {
	subject: String,
}

impl WebFinger {
	pub fn new(subject: &str) -> Self {
		WebFinger {
			aliases: Vec::new(),
			links: Vec::new(),
			subject: subject.to_owned(),
		}
	}
}

pub async fn get(web::Query(subject): web::Query<WebFingerRequest>) -> Result<HttpResponse> {
	let owner = actor::get_from_environment();
	println!("{}", owner.email);
	let web_finger = WebFinger::new(&owner.email);
	println!("{}", web_finger.subject);

	if web_finger.subject != subject.subject {
		return Ok(HttpResponse::build(StatusCode::UNAUTHORIZED).finish());
	}

	return Ok(HttpResponse::Ok().json(web_finger));
}
